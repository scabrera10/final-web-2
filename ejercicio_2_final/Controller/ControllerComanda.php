<?php 
require_once "./View/ViewComanda.php";
require_once "./Model/ModelItem.php";
require_once "./Model/ModelComanda.php";
require_once "./Model/ModelFacturacion.php";



/*Implemente el siguiente requerimiento siguiendo el patrón MVC. No es necesario realizar las vistas, solo controlador(es), modelo(s) y las invocaciones a la vista. 
UNICO
Cerrar una comanda y aplicar un descuento.
Se debe poder cerrar una comanda abierta.
Se deben verificar e informar posibles errores.
Una vez cerrada, se debe generar una entrada en la tabla facturación con el monto total de la factura. 
El descuento debe ser calculado de la siguiente manera:
< $1000 sin descuento
> $1000 descuento del 10%
Se debe imprimir un ticket con el monto total y los items de la comanda, informando el descuento si lo hubiera.
 

*/

class ControllerComanda {
	private $view;	
	private $modelComanda;
	private $modelFacturacion;
	private $modelItem;
	private $logueado;

	function __construct(){
		$this->view = new ViewComanda();
		$this->modelItem = new ModelItem();
		$this->modelComanda = new ModelComanda();
		$this->modelFacturacion = new ModelFacturacion();
	}


	function updateComanda($params = null){
		$id_comanda = $params[':ID'];

		if(isset($id_comanda) && $id_comanda != '') {
			$comanda = $this->model->getComandaById($id_comanda);

			$item = $this->modelItem->getItemByIdComanda($id_comanda);

			if(isset($item) && $item != '') {
				$precio_final = $item->cantidad * $item->precio_unitario;
			} else {
				$this->view->showError("No existe el item");
			}

			if($comanda->cerrada != 'true'){
				//cierro la comanda
				$this->model->updateComanda($id_comanda,true);

				$precio_con_descuento = $precio_final*10/100;


				$dia_actual = date("d");;
				$mes_actual = date("F");
				$año_actual = date("y");

				$mensaje_de_descuento = "Tiene descuento por gastar mas de $1000"


				//Inserto en facturacion dependiendo el monto final
				if($precio_final < 1000) {
					$success = $this->modelFacturacion->InsertFactura($dia_actual,$mes_actual,$año_actual,$precio_final,$id_comanda);

					$this->view->generarTicket($precio_final,$comanda->nro_comanda,$comanda->nro_mesa,$comanda->cerrada);


				} else {
					$success = $this->modelFacturacion->InsertFactura($dia_actual,$mes_actual,$año_actual,$precio_con_descuento,$id_comanda);	

					$this->view->generarTicket($precio_con_descuento,$comanda->nro_comanda,$comanda->nro_mesa,$comanda->cerrada,$mensaje_de_descuento);
				}

			} else {
				$this->view->showError("La comanda ya esta cerrada");
			}

		} else {
			$this->view->showError("La comanda no existe");
		}
	}
		



}