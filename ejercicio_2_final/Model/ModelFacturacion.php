<?php 

class ModelFacturacion {
	
	private $db;

	function __construct() {

		try {
			$this->db = new PDO('mysql:host=localhost;'.'dbname=db_productos;charset=utf8', 'root', ''); 
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

		} catch(Exception $e) {
			var_dump($e);
		}
		return $this->db;

	}	



		function getFacturaByIdComanda($id_comanda) {
			$sentencia = $this->db->prepare("SELECT * FROM facturacion WHERE id_comanda=?");
		    $sentencia->execute(array($id_comanda));
		    return $sentencia->fetch(PDO::FETCH_OBJ);
		}


		//Insertar
		function InsertFactura($dia,$mes,$año,$monto_final,$id_comanda){
		    $sentencia = $this->db->prepare("INSERT INTO facturacion(dia,mes,año,monto_final,id_comanda) VALUES (?,?,?,?,?)");
		    $sentencia->execute(array($dia,$mes,$año,$monto_final,$id_comanda));
		    return $this->db->lastInsertId();
		}




}



 ?>