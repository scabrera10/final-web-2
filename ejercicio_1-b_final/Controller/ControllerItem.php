<?php 
require_once "./View/ViewItem.php";
require_once "./Model/ModelItem.php";
require_once "./Model/ModelComanda.php";
require_once "./logueado/logueado.php";

/*Agregar un ITEM a una comanda
Se debe poder agregar un item a una comanda abierta indicando todos los datos necesarios y cumpliendo los siguientes requerimientos. Informar los errores correspondientes en caso de no cumplirlos.
Controlar posibles errores de carga de datos.
Verificar que el usuario esté logueado.
Verificar que la comanda exista y esté abierta.
Verificar que no esté repetido el item
*/

class ControllerItem {
	private $view;	
	private $modelComanda;
	private $modelItem;
	private $logueado;

	function __construct(){
		$this->view = new ViewItem();
		$this->modelItem = new ModelItem();
		$this->modelComanda = new ModelComanda();
		$this->logueado = new logueado();
	}


	function insertItem(){
		//Chequeo que este logueado
		$this->logueado->chequeoLogueado();

		if(isset($_POST['id_comanda']) && $_POST['id_comanda'] != "")){
			$id_comanda = $_POST['id_comanda'];

			$comanda = $this->model->getComandaById($id_comanda);

			//comanda cerrada = 'true'  comanda  abierta = 'false'
			if(isset($comanda) && $comanda->cerrada != 'true') {

				if(isset($_POST('item')&& $_POST['item'] != "") && iseet($_POST('cantidad') && $_POST['cantidad'] != "") && iseet($_POST('precio_unitario') && $_POST['precio_unitario'] != "")){

					$success = $this->model->InsertItem($_POST['item'],$_POST['cantidad'],$_POST['precio_unitario'],$id_comanda);
				} else{ 
					$this->view->showError("Error al insertar el item");
				}
			} else {
				$this->view->showError("La comanda esta cerrada o no existe");
			}

		} else {
			$this->view->showError("La comanda que quiere insertar el item no existe");
		}	

	}



}