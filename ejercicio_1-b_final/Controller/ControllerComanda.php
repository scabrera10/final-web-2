<?php 
require_once "./View/ViewComandas.php";
require_once "./Model/ModelComanda.php";
require_once "./Model/ModelItem.php";
require_once "./logueado/logueado.php";

/*Agregar un ITEM a una comanda
Se debe poder agregar un item a una comanda abierta indicando todos los datos necesarios y cumpliendo los siguientes requerimientos. Informar los errores correspondientes en caso de no cumplirlos.
Controlar posibles errores de carga de datos.
Verificar que el usuario esté logueado.
Verificar que la comanda exista y esté abierta.
Verificar que no esté repetido el item
*/

class ControllerComanda {
	private $view;	
	private $modelComanda;
	private $modelItem;
	private $logueado;

	function __construct(){
		$this->view = new ViewComanda();
		$this->modelComanda = new ModelComanda();
		$this->modelItem = new ModelItem();
		$this->logueado = new logueado();
	}










}