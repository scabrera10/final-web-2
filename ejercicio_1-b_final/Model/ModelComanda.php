<?php 

class ModelComanda {
	
	private $db;

	function __construct() {

		try {
			$this->db = new PDO('mysql:host=localhost;'.'dbname=db_productos;charset=utf8', 'root', ''); 
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

		} catch(Exception $e) {
			var_dump($e);
		}
		return $this->db;

	}	
		

		function getComandaById($id_comanda) {
			$sentencia = $this->db->prepare("SELECT * FROM comanda WHERE id_comanda=?");
		    $sentencia->execute(array($id_comanda));
		    return $sentencia->fetch(PDO::FETCH_OBJ);
		}

}



 ?>