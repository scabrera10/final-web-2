<?php 
require_once "./View/ViewComanda.php";
require_once "./Model/ModelItem.php";
require_once "./Model/ModelComanda.php";




/*Implemente el siguiente requerimiento siguiendo el patrón MVC. No es necesario realizar las vistas, solo controlador(es), modelo(s) y las invocaciones a la vista. 
Generar una tabla resumen de comandas abiertas
Se debe mostrar una lista de las comandas abiertas, con el detalle de items consumidos hasta el momento.
Se debe informar el monto total de cada item, y el monto total de la mesa hasta el momento.


*/

class ControllerComanda {
	private $view;	
	private $modelComanda;
	private $modelItem;
	private $logueado;

	function __construct(){
		$this->view = new ViewComanda();
		$this->modelItem = new ModelItem();
		$this->modelComanda = new ModelComanda();
	}


	function getComandasAbiertas(){
		$comandas_abiertas = $this->modelComanda->getComandasAbiertas();

		$informacionMesa = new ArrayObjerct();

		$detalle_item = '';
		$cantidad = 0;
		$subtotal = 0;
		$total = 0;

		if(isset($comandas_abiertas)) {
			foreach ($comandas_abiertas as $comanda) {
				$id_comandas = $comanda->id;


				$items = $this->modelItem->getItemByIdComanda($id_comandas);

				foreach ($items as $item) {
					if($id_comandas = $item->id){
						$detalle_item = $item->item;
						$cantidad = $item->cantidad;
						$subtotal = $item->precio_unitario;
						$total = $item->precio_unitario * $item->cantidad;
					}

				$informacionMesa->append(array($comanda->nro_mesa,$detalle_item,$cantidad,$subtotal,$total));	

				}

			}	

			$this->view->ShowInformacionMesa($informacionMesa);
		} else {
			$this->view->showError("Error");
		}
		
	}

}