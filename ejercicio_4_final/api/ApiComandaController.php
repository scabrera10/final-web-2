<?php

require_once "./Model/ComandasModel.php";
require_once "ApiController.php";
require_once "./Model/UserModel.php";


class ApiComandaController extends ApiController {

	private $modelUsers;

	function __construct() {
		parent::__construct();
		$this->model = new ComandaModel();
		$this->view = new ApiView();
		$this->modelUsers = new UserModel();

	}



	public function getComandaByNroMesa($params = null) {
		$users = $this->modelUsers->getUsers();

		if(isset($usser) && $user->usuario == 'normal'){
			$comanda_id = $params[':ID'];
			$comanda = $this->model->getComandaById($comanda_id);
			$this->view->response($comanda,200);
		} else {
			$this->view->response("Error con la acreditacion",404);
		}	
	}


	public function updateTiempoComanda($params = null) {
		$users = $this->modelUsers->getUsers();
			if(isset($usser) && $user->usuario == 'admin'){
				$id_comanda = $params[':ID'];
				$body = $this->getData(); 

				if(isset($body) && $body != "") {
					$comanda = $this->model->updateTiempoComanda($id_comanda, $body->tiempo);	

					$this->view->response('La actualizacion se realizo correctamente',200);
				} else {
					//404
					$this->view->response('No existe',404);	
					
				}
			}else {
				$this->view->response("Error con la acreditacion",404)
			}	
	}


	public function getComandasEnPreparacion($params = null){
		$users = $this->modelUsers->getUsers();
			if(isset($usser) && $user->usuario == 'admin'){
				$comanda_id = $params[':ID'];
				$comanda = $this->model->getComandasEnPreparacion($comanda_id);
				$this->view->response($comanda,200);
			}else {
				$this->view->response("Error con la acreditacion",404)
			}	
	}





}




